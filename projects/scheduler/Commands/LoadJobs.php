<?php

declare(strict_types=1);

namespace FastStore\Scheduler\Commands;

use PhpAmqpLib\Message\AMQPMessage;

class LoadJobs {
  public function execute($argv) {
    $data = implode(' ', array_slice($argv, 1));
    if (empty($data)) {
        $data = "Hello World!";
    }
    $msg = new AMQPMessage($data);

    $this->channel->basic_publish($msg, '', 'hello');

    echo ' [x] Sent ', $data, "\n";
  }
}
