<?php

declare(strict_types=1);

namespace FastStore\Common;

class Configuration {
  private $configurations;

  public function __construct(array $configurations) {
    $this->configurations = $configurations;
  }

  public function get(string $key): ?string {
    $parts = explode('.', $key);
    $value = $this->configurations;

    for($i = 0; $i < count($parts); $i++) {
      $part = $parts[$i];

      if(!array_key_exists($part, $value))
        return null;
      
      $value = $value[$part];
    }

    return $value;
  }
}
