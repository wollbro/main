<?php

declare(strict_types=1);

namespace FastStore\Common\Models;

class Product {
  private $id;

  public function setId(int $id) {
    $this->id = $id;
  }

  public function getId() {
    return $this->id;
  }
}
