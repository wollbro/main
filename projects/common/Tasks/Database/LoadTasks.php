<?php

declare(strict_types=1);

namespace FastStore\Common\Tasks\Database;

trait LoadTasks {
  protected function databaseRebuildDemo() {
    return $this->injector->make(DbRebuildDemo::class)->run();
  }
}
