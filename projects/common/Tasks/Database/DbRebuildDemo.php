<?php

declare(strict_types=1);

namespace FastStore\Api\Tasks\Database;

use Robo\Result;
use Robo\Task\BaseTask;

class DbRebuildDemo extends BaseTask {
  public function run() {
    return Result::success($this);
  }
}
