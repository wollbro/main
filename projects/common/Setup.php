<?php

declare(strict_types=1);

namespace FastStore\Common;

use Auryn\Injector;

class Setup {
  public static function execute(Injector $injector) {
    $data = \yaml_parse_file("../config.yml");
    $injector->define(Configuration::class, [':configurations' => $data]);
    $injector->share(Configuration::class);
  }
}
