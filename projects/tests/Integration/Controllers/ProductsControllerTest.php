<?php

declare(strict_types=1);

namespace FastStore\Api\Tests\Integration\Controllers;

use Auryn\Injector;
use FastStore\Api\Controllers\ProductsController;
use FastStore\Api\System\Setup;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class ProductsControllerTest extends TestCase {
  private $controller;

  public function setUp(): void {
    $injector = new Injector();
    Setup::execute($injector);

    $this->controller = $injector->make(ProductsController::class);
  }

  public function testSendGetRequest() {
    $request = Request::create('');
    $response = $this->controller->get($request);
    $content = json_decode($response->getContent(), true);
    $this->assertCount(2, $content['products']);
  }
}
