<?php

declare(strict_types=1);

namespace FastStore\Tests\Api\Commands\Products;

use FastStore\Api\Commands\Products\GetProducts;
use PHPUnit\Framework\TestCase;

class GetProductsTest extends TestCase {
  private $command;

  public function setUp(): void {
    $this->command = new GetProducts();
  }

  public function testGetAllProducts() {
    $products = $this->command->execute(null);
    $this->assertCount(2, $products);
  }
}
