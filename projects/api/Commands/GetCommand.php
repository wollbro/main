<?php

declare(strict_types=1);

namespace FastStore\Api\Commands;

interface GetCommand {
  public function execute(?int $id): array;
}
