<?php

declare(strict_types=1);

namespace FastStore\Api\Commands\Products;

use FastStore\Api\Commands\GetCommand;
use FastStore\Common\Models\Product;

class GetProducts implements GetCommand {
  public function execute(?int $id): array {
    return [new Product()];
  }
}
