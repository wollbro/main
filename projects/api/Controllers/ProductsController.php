<?php

declare(strict_types=1);

namespace FastStore\Api\Controllers;

use FastStore\Api\Commands\Products\GetProducts;
use FastStore\Api\Exceptions\NotFoundException;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\{Request, Response};

class ProductsController implements Controller {
  private $logger;
  private $getProducts;

  public function __construct(
    Logger $logger,
    GetProducts $getProducts
  ) {
    $this->logger = $logger;
    $this->getProducts = $getProducts;
  }

  public function get(Request $request): Response {
    try {
      $id = $request->attributes->get('id') ?? null;
      $products = $this->getProducts->execute($id);
      
      return new Response(json_encode(['products'=>$products]), 200);
    } catch(NotFoundException $ex) {
      $this->logger->warning($ex->getMessage(), $ex->getTrace());
      return new Response('', 404);
    } catch(\Exception $ex) {
      $this->logger->warning($ex->getMessage(), $ex->getTrace());
      return new Response('', 500);
    }
  }

  public function post(Request $request): Response {
    return new Response('', 501);
  }

  public function put(Request $request): Response {
    return new Response('', 501);
  }

  public function delete(Request $request): Response {
    return new Response('', 501);
  }
}
