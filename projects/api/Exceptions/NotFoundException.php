<?php

declare(strict_types=1);

namespace FastStore\Api\Exceptions;

class NotFoundException extends \Exception {}
