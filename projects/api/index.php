<?php

declare(strict_types=1);

namespace FastStore\Api;

use Auryn\Injector;
use FastStore\Api\Network\Router;
use FastStore\Api\Setup as ApiSetup;
use FastStore\Common\Setup as CommonSetup;
use Symfony\Component\HttpFoundation\Request;

require_once("../vendor/autoload.php");

$injector = new Injector();
ApiSetup::execute($injector);
CommonSetup::execute($injector);

$router = $injector->make(Router::class);
$request = Request::createFromGlobals();
$response = $router->run($request);
$response->headers->set('Content-Type', 'application\json');

$response->send();
