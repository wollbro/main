<?php

declare(strict_types=1);

namespace FastStore\Api;

use Auryn\Injector;
use FastStore\Api\Factories\{ControllerFactory, MatcherFactory};
use Monolog\Handler\ErrorLogHandler;
use Monolog\Logger;

class Setup {
  public static function execute(Injector $injector) {
    $injector->define(ControllerFactory::class, [':injector' => $injector]);
    $injector->define(MatcherFactory::class, [':injector' => $injector]);

    $injector->delegate(Logger::class, function() {
      return new Logger('Api', [
        new ErrorLogHandler(ErrorLogHandler::OPERATING_SYSTEM, LOGGER::DEBUG)
      ]);
    });

    $injector->share(Logger::class);
    $injector->share(ControllerFactory::class);
    $injector->share(MatcherFactory::class);
  }
}
