<?php

declare(strict_types=1);

namespace FastStore\Api\Network;

use FastStore\Api\Exceptions\NotFoundException;
use FastStore\Api\Factories\MatcherFactory;
use FastStore\Api\Network\Matchers\SubPathMatcher;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\{Request, Response};

class Router {
  private $matcherFactory;
  private $logger;

  public function __construct(MatcherFactory $matcherFactory, Logger $logger) {
    $this->matcherFactory = $matcherFactory;
    $this->logger = $logger;
  }

  public function run(Request $request): Response {
    try {
      $action = $request->server->get('REQUEST_METHOD');
      $controller = null;

      $matchers = [
        SubPathMatcher::class
      ];
      foreach($matchers as $matcherName) {
        $matcher = $this->matcherFactory->create($matcherName);
        if($matcher === null)
          continue; 

        $controller = $matcher->find($request);
        if($controller === null)
          continue;

        if(!method_exists($controller, $action))
          continue;

        return $controller->$action($request);
      }

      throw new NotFoundException();
    } catch(NotFoundException $ex) {
      $this->logger->warning($ex->getMessage(), $ex->getTrace());
      return new Response('', 404);
    } catch(\Exception $ex) {
      $this->logger->warning($ex->getMessage(), $ex->getTrace());
      return new Response($ex->getMessage(), 500);
    }
  }
}
