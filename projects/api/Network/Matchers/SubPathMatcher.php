<?php

declare(strict_types=1);

namespace FastStore\Api\Network\Matchers;

use FastStore\Api\Controllers\Controller;
use FastStore\Api\Factories\ControllerFactory;
use Symfony\Component\HttpFoundation\{Request};

class SubPathMatcher implements Matcher {
  private $controllerFactory;

  public function __construct(ControllerFactory $controllerFactory) {
    $this->controllerFactory = $controllerFactory;
  }

  public function find(Request $request): ?Controller {
    $parts = explode('/', $request->getPathInfo());
    $parts = array_filter($parts);
    $parts = array_values($parts);

    $controller = $this->controllerFactory->create($parts[0]);

    if($controller === null)
      return null;

    return $controller;
  }
}
