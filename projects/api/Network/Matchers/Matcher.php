<?php

declare(strict_types=1);

namespace FastStore\Api\Network\Matchers;

use FastStore\Api\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface Matcher {
  public function find(Request $request): ?Controller;
}
