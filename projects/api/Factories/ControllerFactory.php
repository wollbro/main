<?php

declare(strict_types=1);

namespace FastStore\Api\Factories;

use Auryn\Injector;

class ControllerFactory implements Factory {
  private $injector;

  public function __construct(Injector $injector) {
    $this->injector = $injector;
  }

  public function create(string $name) {
    $name = $this->normalizeControllerName($name);

    $fullPath = "\\FastStore\\Api\\Controllers\\{$name}Controller";
    if(!class_exists($fullPath))
      return null;

    return $this->injector->make($fullPath);
  }

  private function normalizeControllerName(string $name): string {
    $name = ucfirst($name);
    return $name;
  }
}
