<?php

declare(strict_types=1);

namespace FastStore\Api\Factories;

use Auryn\Injector;

class MatcherFactory implements Factory {
  private $injector;

  public function __construct(Injector $injector) {
    $this->injector = $injector;
  }

  public function create(string $name) {
    if(!class_exists($name))
      return null;

    return $this->injector->make($name);
  }
}
