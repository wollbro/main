<?php

declare(strict_types=1);

namespace FastStore\Api\Factories;

interface Factory {
  public function create(string $name);
}
