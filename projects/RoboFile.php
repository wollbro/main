<?php

use Auryn\Injector;
use FastStore\Api\System\Setup;

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see https://robo.li/
 */
class RoboFile extends \Robo\Tasks {
  use \FastStore\Common\Tasks\Database\LoadTasks;

  private $injector;

  public function __construct() {
    $this->injector = new Injector();
    Setup::execute($this->injector);
  }

  public function dbRebuildDemo() {
    $this->databaseRebuildDemo();
  }

  public function testAll() {
    $this->taskExec('src/vendor/bin/phpunit -c /api/phpunit.xml tests')->run();
  }

  public function testUnit() {
    $this->taskExec('src/vendor/bin/phpunit -c /api/phpunit.xml tests/Unit')->run();
  }

  public function testIntegration() {
    $this->taskExec('src/vendor/bin/phpunit -c /api/phpunit.xml tests/Integration')->run();
  }

  public function testFile(string $filePath) {
    $this->taskExec("src/vendor/bin/phpunit -c /api/phpunit.xml $filePath")->run();
  }
}
